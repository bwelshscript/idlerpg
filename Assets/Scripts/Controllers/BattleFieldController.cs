﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IdleRPG {
	public class BattleFieldController : MonoBehaviour
	{

		// singleton instance can be destroyed on load as this class should only exist in the battlefield scene, unless the class requirements change in the future
		private static BattleFieldController _instance;
		public static BattleFieldController Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<BattleFieldController>();
				}

				return _instance;
			}
		}

		public RectTransform CharacterInfoParent, BF_Rear_Layer, BF_Mid_Layer, BF_Front_Layer, BossHPAnchor;

		public List<Actor> ActorList;
		public List<Actor> DeadActorList;
		public List<Actor> ActionQueue;

		public enum TEAM { PARTY, NOTPARTY, NONE}

		int timerCooldown = SETTINGS.TURNTIMER_UPDATE_SPEED;
		int UIUpdateTimer = SETTINGS.UI_UPDATE_TICKRATE;

		public void ActorReport(Actor actor)
		{
			ActorList.Add(actor);
		}

		public void ActorRemove(Actor actor)
		{
			DeadActorList.Add(actor);
			//ActorList.Remove(actor);
		}

		public void EnqueueAction(Actor actor)
		{
			ActionQueue.Add(actor);
		}

		public Actor GetEnemyTargetRandom(TEAM myTeam)
		{
			Actor returnActor = null;

			List<int> indexArr = new List<int>();
			int index = 0;
			int retries = 40;

			while (retries > 0 && (returnActor == null || DeadActorList.Contains(returnActor)))
			{
				switch (myTeam)
				{
					case TEAM.PARTY:

						// get indexes of all enemy actors, then randomly choose target from those actors.
						for (int i = 0; i < ActorList.Count; i++)
						{
							if (ActorList[i].GetType() == typeof(EnemyActor))
							{
								indexArr.Add(i);
							}
						}

						if (indexArr.Count == 0) { Debug.LogWarning(" No eneimies to fight"); return null; }
						index = UnityEngine.Random.Range(0, indexArr.Count);

						returnActor = (ActorList[indexArr[index]]);
						break;

					case TEAM.NOTPARTY:

						// get indexes of all friendle/party actors, then randomly choose target from those actors.
						for (int i = 0; i < ActorList.Count; i++)
						{
							if (ActorList[i].GetType() == typeof(FriendlyActor))
							{
								indexArr.Add(i);
							}
						}

						if (indexArr.Count == 0) { Debug.LogWarning(" No eneimies to fight"); return null; }
						index = UnityEngine.Random.Range(0, indexArr.Count);

						returnActor = (ActorList[indexArr[index]]);
						break;

					case TEAM.NONE:
						// for use with beserked or confused characters
						index = UnityEngine.Random.Range(0, ActorList.Count);
						if (ActorList.Count == 1) { Debug.LogWarning(" No eneimies to fight"); return null; }
						returnActor = (ActorList[index]);
						break;
					default:
						break;
				}
				retries--;
			}		
			if (returnActor == null) { Debug.LogWarning("no targets available"); }
			return returnActor;
		}

		private void Update()
		{
			timerCooldown--;

			if (timerCooldown <= 0)
			{
				timerCooldown = SETTINGS.TURNTIMER_UPDATE_SPEED;

				//clear dead actors here
				MoveDeadActorsToDAL();

				if (!RunActions())
				{
					return;
				}

				if (ActionQueue.Count == 0)
				{
					foreach (Actor actor in ActorList)
					{
						actor.TurnUpdate();
					}


				}
			}

			if (UIUpdateTimer <= 0)
			{
				foreach (Actor actor in ActorList)
				{
					actor.UIUpdate();
				}

				UIUpdateTimer = SETTINGS.UI_UPDATE_TICKRATE;
			}
			else
			{
				UIUpdateTimer--;
			}
		}

		private void MoveDeadActorsToDAL()
		{
			foreach (Actor actor in DeadActorList)
			{
				if (ActorList.Contains(actor))
				{
					ActorList.Remove(actor);
				}
			}
		}

		private bool RunActions()
		{

			for (int i = ActionQueue.Count -1; i >=0 ; i--)
			{
				// run each do action, stop if the doaction returns false
				if (!ActionQueue[i].DoAction())
				{
					return false;
				}
				else
				{
					ActionQueue.RemoveAt(i);
				}
			}

			if (ActionQueue.Count <= 0)
			{
				return true;
			}

			return false;
		}
	}
}