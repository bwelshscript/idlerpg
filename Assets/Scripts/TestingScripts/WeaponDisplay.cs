﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IdleRPG;

public class WeaponDisplay : MonoBehaviour
{
	public GameObject WeaponButtonPrefab;
	public InputField levelInput;
	public Button generateButton;

	public Text titleText, descriptionText;

	public Color[] textColours = new Color[8];
	public Color godlyBGcol, extraplanBGCol, defaultColour;
	public Image BGIMAGE;

	public void PopulateGrid(int amount)
	{
		int newLevel = -1;
		int.TryParse(levelInput.text, out newLevel);
		if (newLevel <= 0) { newLevel = -1; }

		for (int i = 0; i < amount; i++)
		{
			GameObject temp = Instantiate(WeaponButtonPrefab, this.transform);
			temp.GetComponent<WeaponButtonBehaviour>().Init(newLevel);
			temp.GetComponent<Button>().onClick.AddListener(delegate { SelectWeapon(temp); });
		}
	}

	public void ClearGrid()
	{
		Transform[] children = GetComponentsInChildren<Transform>(true);
		for (int i = 1; i < children.Length; i++)
		{
			Destroy(children[i].gameObject);
		}

		titleText.text = "";
		descriptionText.text = "";
	}

	public void SelectWeapon(GameObject weaponContainer)
	{
		Weapon weapon = weaponContainer.GetComponent<WeaponButtonBehaviour>().weapon;

		if (weapon == null) { Debug.LogWarning("no weapon on button"); return; }

		titleText.text = weapon.name;

		Color textCol = Color.black;

		BGIMAGE.color = defaultColour;

		switch (weapon.rarity)
		{
			case Item.RarityLevelEnum.NULL:
				break;
			case Item.RarityLevelEnum.common:
				textCol = textColours[0];
				break;
			case Item.RarityLevelEnum.uncommon:
				textCol = textColours[1];
				break;
			case Item.RarityLevelEnum.rare:
				textCol = textColours[2];
				break;
			case Item.RarityLevelEnum.veryRare:
				textCol = textColours[3];
				break;
			case Item.RarityLevelEnum.Legendary:
				textCol = textColours[4];
				break;
			case Item.RarityLevelEnum.Epic:
				textCol = textColours[5];
				break;
			case Item.RarityLevelEnum.Godly:
				textCol = textColours[6];
				BGIMAGE.color = godlyBGcol;
				break;
			case Item.RarityLevelEnum.Extraplanar:
				textCol = textColours[7];
				BGIMAGE.color = extraplanBGCol;
				break;
			default:
				break;
		
	}

		titleText.color = textCol;

		descriptionText.text = WeaponStatStringBuilder.GetComponentString(weapon);
	}
}
