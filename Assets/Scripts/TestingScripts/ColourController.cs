﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ColourController : MonoBehaviour
{
	public bool SetColours = false, getchildren = false;
	public ColourSetter[] colourChildren;

	public Color[] colours;

	public enum ColourGroups
	{
		Group1, Group2, Group3, Group4, Group5, Group6 , Group7, Group8
	}

	[SerializeField, GetSet("colScheme")]
	private COLOURSCHEME _colScheme;

	public COLOURSCHEME colScheme { get { return _colScheme; } set { _colScheme = value; SetColoursFromScheme(); } }



	// Start is called before the first frame update
	void Init()
    {
		colourChildren = GetComponentsInChildren<ColourSetter>();
    }


	void SetColoursFromScheme()
	{
		for (int i = 0; i < colours.Length; i++)
		{
			colours[i] = _colScheme.colours[i];
		}
	}


	private void Update()
	{
		if (SetColours)
		{
			SetColoursFromScheme();

			SetColours = false;
			foreach (ColourSetter child in colourChildren)
			{
				child.SetCol();
			}
		}
		if (getchildren)
		{
			getchildren = false;
			Init();
		}
	}

	public Color EvaluateColour( ColourGroups colourGroup )
	{
		int index = (int)colourGroup;
		if 
			(index >= colours.Length) { return Color.black; }

		return colours[index];
	}
}
