﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ColourSetter : MonoBehaviour
{
	public Color myCol;



	public ColourController parentColCont;


	[SerializeField, GetSet("MyColGroup")]
	private ColourController.ColourGroups _ColGroup = new ColourController.ColourGroups();
	 Image image;

	public ColourController.ColourGroups MyColGroup {
		get { return _ColGroup; }
		set
		{
			_ColGroup = value;
			myCol = parentColCont.EvaluateColour(_ColGroup);

			if (image == null) { image = GetComponent<Image>(); }

			image.color = myCol;


		}
	}

	public void SetCol()
	{
		myCol = parentColCont.EvaluateColour(_ColGroup);

		if (image == null) { image = GetComponent<Image>(); }

		image.color = myCol;
	}
}
