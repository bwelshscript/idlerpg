﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IdleRPG;

public class WeaponButtonBehaviour : MonoBehaviour
{
	Text buttonNameDisp;
	Button button;
	public Weapon weapon;
	int generationLevel = -1;

	public Color[] textColours = new Color[8];
	public Color godlyBGcol, extraplanBGCol;

	public void Init(int _generationLevel = -1)
	{
		button = GetComponent<Button>();
		buttonNameDisp = GetComponentInChildren<Text>();

		generationLevel = _generationLevel;

		weapon = new Weapon();

		if (generationLevel > -1)
		{
			weapon = WeaponGenerator.GetWeaponAtLevel(generationLevel);
		}

		else
		{
			weapon = WeaponGenerator.GetRandomWeapon();
		}

		buttonNameDisp.text = weapon.shortName;

		Color textCol = Color.black;

		switch (weapon.rarity)
		{
			case Item.RarityLevelEnum.NULL:
				break;
			case Item.RarityLevelEnum.common:
				textCol = textColours[0];
				break;
			case Item.RarityLevelEnum.uncommon:
				textCol = textColours[1];
				break;
			case Item.RarityLevelEnum.rare:
				textCol = textColours[2];
				break;
			case Item.RarityLevelEnum.veryRare:
				textCol = textColours[3];
				break;
			case Item.RarityLevelEnum.Legendary:
				textCol = textColours[4];
				break;
			case Item.RarityLevelEnum.Epic:
				textCol = textColours[5];
				break;
			case Item.RarityLevelEnum.Godly:
				textCol = textColours[6];
				button.image.color = godlyBGcol;
				break;
			case Item.RarityLevelEnum.Extraplanar:
				textCol = textColours[7];
				button.image.color = extraplanBGCol;
				break;
			default:
				break;
		}

		buttonNameDisp.color = textCol;
	}
}
