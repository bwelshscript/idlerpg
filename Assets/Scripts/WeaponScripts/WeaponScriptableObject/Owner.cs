﻿using UnityEngine;

namespace IdleRPG
{
	public class Owner : ScriptableObject
	{
		public Weapon.OwnerEnum owner;
		public bool isNegative = false;

		// add in ability to cast special effects here
	}
}
