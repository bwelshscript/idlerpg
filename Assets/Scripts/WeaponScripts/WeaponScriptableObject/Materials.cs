﻿using UnityEngine;
namespace IdleRPG
{
	public class Materials : ScriptableObject
	{
		public Item.MaterialsEnum material;
		public float WeightMult = 1;
		public float SpeedMult = 1;
		public float HealthMult = 1;
		public bool canRegen = false;
	}
}
