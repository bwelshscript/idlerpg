﻿using UnityEngine;
namespace IdleRPG
{
	public class Prefix : ScriptableObject
	{
		public Weapon.PrefixEnum prefix;
		public int DamageMin = 1;
		public int DamageMax = 1;
		public Item.ElementsEnum damageElement;
		public bool isNegative = false;
		public bool addsNewDamage = false;
		public bool modifiesExistingDamage = false;
		public float damageModifier = 1;
	}
}