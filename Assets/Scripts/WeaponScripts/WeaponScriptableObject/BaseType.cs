﻿using UnityEngine;
namespace IdleRPG
{

	public class BaseType : ScriptableObject
	{

		public Weapon.BaseTypeEnum baseType = Weapon.BaseTypeEnum.NULL;

		public int Health = 100;
		public int DamageMin = 1;
		public int DamageMax = 1;
		public int Weight = 5;
		public int swingSpeed = 1;
		public int reach = 1;
		public bool twoHanded = false;
		public bool canBleed = false;
		public int bleedChance = 0;
	}
}
