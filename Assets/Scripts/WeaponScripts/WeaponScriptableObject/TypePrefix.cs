﻿
using UnityEngine;
namespace IdleRPG
{
	public class TypePrefix : ScriptableObject
	{
		public Weapon.TypePrefixEnum typePrefix;
		public float reachMult = 1;
		public bool twoHanded = false;
		public bool canBleed = false;
		public int bleedChance = 0;
		public int agedLevel = 1;
		public bool mundane = false;
		public bool martial = false;
		public bool isNegative = false;
		public float speedMult = 1;
		public float weightMult = 1;
		public float damageMult = 1;
		public float healthMult = 1;

	}

}