﻿using UnityEngine;

namespace IdleRPG
{
	public class PrePrefix : ScriptableObject
	{
		public Weapon.PrePrefixEnum prePrefix;
		public float effectMult = 1;
		public bool isNegative = false;
	}
}