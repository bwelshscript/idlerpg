﻿using UnityEngine;

namespace IdleRPG
{
	public class RarityLevel : ScriptableObject
	{
		public Item.RarityLevelEnum rarityLevel;

		public float baseStatMultiplier = 1;
	}
}