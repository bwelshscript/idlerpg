﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Text;

namespace IdleRPG
{
	public static class WeaponGenerator
	{
		// generates a weapon on request, should always go through this class to get a weapon.
		// contains ability to just generate a random weapon, a weapon at specific level and a weapon at specific level with specific parts.

		public static Weapon GetRandomWeapon()
		{
			return GetWeaponAtLevel(UnityEngine.Random.Range(1, SETTINGS.MAX_LEVEL));
		}

		public static Weapon GetWeaponAtLevel(int level = -1, Item.RarityLevelEnum rarityLevel = Item.RarityLevelEnum.common)
		{
			// Determine random parts for the weapon, based off weapon level
			Weapon.BaseTypeEnum baseType = Weapon.BaseTypeEnum.NULL;
			Weapon.TypePrefixEnum typePrefix = Weapon.TypePrefixEnum.NULL;
			Item.MaterialsEnum material = Item.MaterialsEnum.NULL;
			Weapon.PrefixEnum prefix = Weapon.PrefixEnum.NULL;
			Weapon.PrePrefixEnum prePrefix = Weapon.PrePrefixEnum.NULL;
			Weapon.OwnerEnum owner = Weapon.OwnerEnum.NULL;
			Weapon.SuffixEnum suffix = Weapon.SuffixEnum.NULL;


			return GetWeaponAtLevelSpecifyParts(level, rarityLevel, baseType, typePrefix, material, prefix, prePrefix, owner, suffix);
		}

		public static Weapon GetWeaponAtLevelSpecifyParts(int level = -1,
														  Item.RarityLevelEnum rarityLevel = Item.RarityLevelEnum.common,
														  Weapon.BaseTypeEnum baseType = Weapon.BaseTypeEnum.NULL,
														  Weapon.TypePrefixEnum typePrefix = Weapon.TypePrefixEnum.NULL,
														  Item.MaterialsEnum material = Item.MaterialsEnum.NULL,
														  Weapon.PrefixEnum prefix = Weapon.PrefixEnum.NULL,
														  Weapon.PrePrefixEnum prePrefix = Weapon.PrePrefixEnum.NULL,
														  Weapon.OwnerEnum owner = Weapon.OwnerEnum.NULL,
														  Weapon.SuffixEnum suffix = Weapon.SuffixEnum.NULL
														 )
		{

			// define rarity level

			switch (level)
			{
				case int n when (n <= 10):
					{
						rarityLevel = WeightedRandomizer.From(SETTINGS.levels1to10).TakeOne();
						break;
					}
				case int n when (n > 10 && n <= 31):
					{
						rarityLevel = WeightedRandomizer.From(SETTINGS.levels11to31).TakeOne();
						break;
					}
				case int n when (n > 31 && n <= 51):
					{
						rarityLevel = WeightedRandomizer.From(SETTINGS.levels31to51).TakeOne();
						break;
					}
				case int n when (n > 51 && n <= 71):
					{
						rarityLevel = WeightedRandomizer.From(SETTINGS.levels51to71).TakeOne();
						break;
					}
				case int n when (n > 71 && n <= 101):
					{
						rarityLevel = WeightedRandomizer.From(SETTINGS.levels71to101).TakeOne();
						break;
					}

				default:
					break;
			}


			// Determine stats that are not already set here
			int max = 0;

			if (baseType == Weapon.BaseTypeEnum.NULL)
			{
				max = Enum.GetValues(typeof(Weapon.BaseTypeEnum)).Length;

				baseType = (Weapon.BaseTypeEnum)UnityEngine.Random.Range(1, max);
			}

			if (typePrefix == Weapon.TypePrefixEnum.NULL && (UnityEngine.Random.Range(0, 100) > 40))
			{
				max = Enum.GetValues(typeof(Weapon.TypePrefixEnum)).Length;

				typePrefix = (Weapon.TypePrefixEnum)UnityEngine.Random.Range(1, max);
			}

			if (material == Item.MaterialsEnum.NULL)
			{
				max = Enum.GetValues(typeof(Item.MaterialsEnum)).Length;

				material = (Item.MaterialsEnum)UnityEngine.Random.Range(1, max);
			}

			if (prefix == Weapon.PrefixEnum.NULL && (UnityEngine.Random.Range(0, 100) > 20))
			{
				max = Enum.GetValues(typeof(Weapon.PrefixEnum)).Length;

				prefix = (Weapon.PrefixEnum)UnityEngine.Random.Range(1, max);
			}

			if (prePrefix == Weapon.PrePrefixEnum.NULL && (UnityEngine.Random.Range(0, 100) > 60))
			{
				max = Enum.GetValues(typeof(Weapon.PrePrefixEnum)).Length;

				prePrefix = (Weapon.PrePrefixEnum)UnityEngine.Random.Range(1, max);
			}

			if (owner == Weapon.OwnerEnum.NULL && (UnityEngine.Random.Range(0, 100) > 85))
			{
				max = Enum.GetValues(typeof(Weapon.OwnerEnum)).Length;

				owner = (Weapon.OwnerEnum)UnityEngine.Random.Range(1, max);
			}

			if (suffix == Weapon.SuffixEnum.NULL && (UnityEngine.Random.Range(0, 100) > 89))
			{
				max = Enum.GetValues(typeof(Weapon.SuffixEnum)).Length;

				suffix = (Weapon.SuffixEnum)UnityEngine.Random.Range(1, max);
			}

			return new Weapon(level, rarityLevel, baseType, typePrefix, material, prefix, prePrefix, owner, suffix);
		}

#if UNITY_EDITOR
		[MenuItem("Tools/Weapons/Create Scriptable Objects")]
		static void CreateScriptables()
		{
			if (!Directory.Exists("Assets/ScriptableObjects/Weapons/WeaponPrefixes/BaseType"))
			{ Directory.CreateDirectory("Assets/ScriptableObjects/Weapons/WeaponPrefixes/BaseType"); }

			for (int i = 1; i < Enum.GetValues(typeof(Weapon.BaseTypeEnum)).Length; i++)
			{
				// CHECK IF ASSET ALREADY EXISTS
				if (AssetDatabase.LoadAssetAtPath(("Assets/ScriptableObjects/Weapons/WeaponPrefixes/BaseType/" + ((Weapon.BaseTypeEnum)i).ToString() + ".asset"), typeof (BaseType)) == null)
				{
					BaseType type = BaseType.CreateInstance<BaseType>();
					type.baseType = (Weapon.BaseTypeEnum)i;
					type.Health = 100;
					type.reach = 1;
					type.swingSpeed = 1;
					type.Weight = 5;
					AssetDatabase.CreateAsset(type, "Assets/ScriptableObjects/Weapons/WeaponPrefixes/BaseType/" + ((Weapon.BaseTypeEnum)i).ToString() + ".asset");
				}
			}

			if (!Directory.Exists("Assets/ScriptableObjects/Weapons/WeaponPrefixes/TypePrefix"))
			{ Directory.CreateDirectory("Assets/ScriptableObjects/Weapons/WeaponPrefixes/TypePrefix"); }

			for (int i = 1; i < Enum.GetValues(typeof(Weapon.TypePrefixEnum)).Length; i++)
			{
				// CHECK IF ASSET ALREADY EXISTS
				if (AssetDatabase.LoadAssetAtPath(("Assets/ScriptableObjects/Weapons/WeaponPrefixes/TypePrefix/" + ((Weapon.TypePrefixEnum)i).ToString() + ".asset"), typeof(TypePrefix)) == null)
				{
					TypePrefix type = TypePrefix.CreateInstance<TypePrefix>();
					type.typePrefix = (Weapon.TypePrefixEnum)i;
					type.reachMult = 1;
					type.agedLevel = 1;
					AssetDatabase.CreateAsset(type, "Assets/ScriptableObjects/Weapons/WeaponPrefixes/TypePrefix/" + ((Weapon.TypePrefixEnum)i).ToString() + ".asset");
				}
			}

			if (!Directory.Exists("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Prefix"))
			{ Directory.CreateDirectory("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Prefix"); }

			for (int i = 1; i < Enum.GetValues(typeof(Weapon.PrefixEnum)).Length; i++)
			{
				if (AssetDatabase.LoadAssetAtPath(("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Prefix/" + ((Weapon.PrefixEnum)i).ToString() + ".asset"), typeof(Prefix)) == null)
				{
					Prefix type = Prefix.CreateInstance<Prefix>();
					type.prefix = (Weapon.PrefixEnum)i;
					type.damageElement = Item.ElementsEnum.Physical;
					AssetDatabase.CreateAsset(type, "Assets/ScriptableObjects/Weapons/WeaponPrefixes/Prefix/" + ((Weapon.PrefixEnum)i).ToString() + ".asset");
				}
			}

			if (!Directory.Exists("Assets/ScriptableObjects/Weapons/WeaponPrefixes/PrePrefix"))
			{ Directory.CreateDirectory("Assets/ScriptableObjects/Weapons/WeaponPrefixes/PrePrefix"); }

			for (int i = 1; i < Enum.GetValues(typeof(Weapon.PrePrefixEnum)).Length; i++)
			{
				if (AssetDatabase.LoadAssetAtPath(("Assets/ScriptableObjects/Weapons/WeaponPrefixes/PrePrefix/" + ((Weapon.PrePrefixEnum)i).ToString() + ".asset"), typeof(PrePrefix)) == null)
				{
					PrePrefix type = PrePrefix.CreateInstance<PrePrefix>();
					type.prePrefix = (Weapon.PrePrefixEnum)i;
					type.effectMult = 1;
					AssetDatabase.CreateAsset(type, "Assets/ScriptableObjects/Weapons/WeaponPrefixes/PrePrefix/" + ((Weapon.PrePrefixEnum)i).ToString() + ".asset");
				}
			}

			if (!Directory.Exists("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Owner"))
			{ Directory.CreateDirectory("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Owner"); }

			for (int i = 1; i < Enum.GetValues(typeof(Weapon.OwnerEnum)).Length; i++)
			{
				if (AssetDatabase.LoadAssetAtPath(("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Owner/" + ((Weapon.OwnerEnum)i).ToString() + ".asset"), typeof(Owner)) == null)
				{
					Owner type = Owner.CreateInstance<Owner>();
					type.owner = (Weapon.OwnerEnum)i;
					AssetDatabase.CreateAsset(type, "Assets/ScriptableObjects/Weapons/WeaponPrefixes/Owner/" + ((Weapon.OwnerEnum)i).ToString() + ".asset");
				}
			}

			if (!Directory.Exists("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Suffix"))
			{ Directory.CreateDirectory("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Suffix"); }

			for (int i = 1; i < Enum.GetValues(typeof(Weapon.SuffixEnum)).Length; i++)
			{
				if (AssetDatabase.LoadAssetAtPath(("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Suffix/" + ((Weapon.SuffixEnum)i).ToString() + ".asset"), typeof(Suffix)) == null)
				{
					Suffix type = Suffix.CreateInstance<Suffix>();
					type.suffix = (Weapon.SuffixEnum)i;
					AssetDatabase.CreateAsset(type, "Assets/ScriptableObjects/Weapons/WeaponPrefixes/Suffix/" + ((Weapon.SuffixEnum)i).ToString() + ".asset");
				}
			}

			if (!Directory.Exists("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Materials"))
			{ Directory.CreateDirectory("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Materials"); }

			for (int i = 1; i < Enum.GetValues(typeof(Item.MaterialsEnum)).Length; i++)
			{
				if (AssetDatabase.LoadAssetAtPath(("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Materials/" + ((Item.MaterialsEnum)i).ToString() + ".asset"), typeof(Materials)) == null)
				{
					Materials type = Materials.CreateInstance<Materials>();
					type.material = (Item.MaterialsEnum)i;
					type.HealthMult = 1;
					type.SpeedMult = 1;
					type.WeightMult = 1;
					AssetDatabase.CreateAsset(type, "Assets/ScriptableObjects/Weapons/WeaponPrefixes/Materials/" + ((Item.MaterialsEnum)i).ToString() + ".asset");
				}
			}

			if (!Directory.Exists("Assets/ScriptableObjects/Weapons/WeaponPrefixes/RarityLevel"))
			{ Directory.CreateDirectory("Assets/ScriptableObjects/Weapons/WeaponPrefixes/RarityLevel"); }

			for (int i = 1; i < Enum.GetValues(typeof(Item.RarityLevelEnum)).Length; i++)
			{
				if (AssetDatabase.LoadAssetAtPath(("Assets/ScriptableObjects/Weapons/WeaponPrefixes/RarityLevel/" + ((Item.RarityLevelEnum)i).ToString() + ".asset"), typeof(RarityLevel)) == null)
				{
					RarityLevel type = RarityLevel.CreateInstance<RarityLevel>();
					type.rarityLevel = (Item.RarityLevelEnum)i;
					type.baseStatMultiplier = 1;
					AssetDatabase.CreateAsset(type, "Assets/ScriptableObjects/Weapons/WeaponPrefixes/RarityLevel/" + ((Item.RarityLevelEnum)i).ToString() + ".asset");
				}
			}

		}

#endif
	}

	[System.Serializable]
	public class Weapon : Item
	{
		// the object that is built, and contains all the specific values and behaviours of a weapon
		BaseTypeEnum baseType;
		TypePrefixEnum typePrefix;
		PrefixEnum prefix;
		PrePrefixEnum prePrefix;
		OwnerEnum owner;
		SuffixEnum suffix;
		Item.MaterialsEnum material;
		public Item.RarityLevelEnum rarity;

		public int WeaponHealth = 100;
		public int Weight = 5;
		public int SwingSpeed = 1;
		public int Reach = 1;
		public int agedLevel = 1;
		public int bleedChance = 0;
		public int levelStatMultiplier = 1; // not set by chance or prefix

		public bool twoHanded = false;
		public bool canBleed = false;
		public bool canRegen = false;
		public bool mundane = false;
		public bool martial = false;
		
		// this is all damage types and values added together for easy display, not for working out damage
		public Vector2Int damageSummary = new Vector2Int(0, 0);
		public List<DamageInfo> damageInformation = new List<DamageInfo>();

		//contains all the different types of damage dealt directly by the weapon (no secondary effects, like bleed ticks or cast on hit spells)
		Dictionary<Item.ElementsEnum, Vector2Int> DamageTypesAndValues = new Dictionary<ElementsEnum, Vector2Int>() { { ElementsEnum.Physical, new Vector2Int(1,1) } };

		public Weapon(int _level = -1,
						Item.RarityLevelEnum _rarityLevel = Item.RarityLevelEnum.common,
						BaseTypeEnum _baseType = 0,
						TypePrefixEnum _typePrefix = 0,
						Item.MaterialsEnum _material = 0,
						PrefixEnum _prefix = 0,
						PrePrefixEnum _prePrefix = 0,
						OwnerEnum _owner = 0,
						SuffixEnum _suffix = 0
					 )
		{
			baseType = _baseType;
			typePrefix = _typePrefix;
			material = _material;
			prefix = _prefix;
			prePrefix = _prePrefix;
			owner = _owner;
			suffix = _suffix;
			itemLevel = _level;
			rarity = _rarityLevel;

			// for testing
			Init();
		}

		private void Init()
		{

			ApplyPrefixValues();
			ApplylevelMult();
			BuildName();

			for (int i = 0; i < DamageTypesAndValues.Count; i++)
			{
				if (DamageTypesAndValues[(Item.ElementsEnum)i].x > 0|| DamageTypesAndValues[(Item.ElementsEnum)i].y > 0)
				{
					// add to total, for simple display purposes
					damageSummary += DamageTypesAndValues[(Item.ElementsEnum)i];
					// add to breakdown of damage types, for use with display and actual damage
					damageInformation.Add(new DamageInfo((Item.ElementsEnum)i, DamageTypesAndValues[(Item.ElementsEnum)i].x, DamageTypesAndValues[(Item.ElementsEnum)i].y));
				}
			}
		}

		public void ApplyPrefixValues()
		{
			if (baseType != BaseTypeEnum.NULL)
			{
				BaseType baseTypeSO = AssetDatabase.LoadAssetAtPath<BaseType>("Assets/ScriptableObjects/Weapons/WeaponPrefixes/BaseType/" + baseType.ToString() + ".asset");

				WeaponHealth = baseTypeSO.Health = 100;
				DamageTypesAndValues[ElementsEnum.Physical] = new Vector2Int(baseTypeSO.DamageMin, baseTypeSO.DamageMax);

				Weight = baseTypeSO.Weight;
				SwingSpeed = baseTypeSO.swingSpeed;
				Reach = baseTypeSO.reach;
				twoHanded = baseTypeSO.twoHanded;
				canBleed = baseTypeSO.canBleed;
				bleedChance = baseTypeSO.bleedChance;
			}

			if (typePrefix != TypePrefixEnum.NULL)
			{
				TypePrefix typePrefixSO = AssetDatabase.LoadAssetAtPath<TypePrefix>("Assets/ScriptableObjects/Weapons/WeaponPrefixes/TypePrefix/" + typePrefix.ToString() + ".asset");

				Reach = (int)(Reach * typePrefixSO.reachMult);
				twoHanded = (twoHanded || typePrefixSO.twoHanded);
				canBleed = (canBleed || typePrefixSO.canBleed);
				bleedChance += typePrefixSO.bleedChance;
				agedLevel = typePrefixSO.agedLevel;
				mundane = (mundane || typePrefixSO.mundane);
				martial = (martial || typePrefixSO.martial);




				if (typePrefixSO.typePrefix == TypePrefixEnum.ancient || typePrefixSO.typePrefix == TypePrefixEnum.old)
				{   
					// check if material is wood or bone in case of old or ancient prefixes
					Materials materialSO = AssetDatabase.LoadAssetAtPath<Materials>("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Materials/" + material.ToString() + ".asset");

					if (materialSO.material != MaterialsEnum.bone && materialSO.material != MaterialsEnum.wood)
					{
						// not the right materials, do nothing
					}
					else
					{
						// work out new modifiers, then work out damage
						if (typePrefixSO.typePrefix == TypePrefixEnum.ancient)
						{
							if (materialSO.material != MaterialsEnum.bone)
							{
								typePrefixSO.speedMult	+= 1.4f;
								typePrefixSO.weightMult += 2;
								typePrefixSO.healthMult += 2.5f;
								typePrefixSO.damageMult += 1.95f;
							}
							else if (materialSO.material != MaterialsEnum.wood)
							{
								typePrefixSO.speedMult	+= 2;
								typePrefixSO.weightMult += 2.5f;
								typePrefixSO.healthMult += 3;
								typePrefixSO.damageMult += 1.75f;
							}
						}
						else if (typePrefixSO.typePrefix == TypePrefixEnum.old)
						{
							if (materialSO.material != MaterialsEnum.bone)
							{
								typePrefixSO.speedMult	-= 0.8f;
								typePrefixSO.weightMult -= 1.4f;
								typePrefixSO.healthMult -= 1.5f;
								typePrefixSO.damageMult -= 0.25f;
							}
							else if (materialSO.material != MaterialsEnum.wood)
							{
								typePrefixSO.speedMult	+= 0.45f;
								typePrefixSO.weightMult += 0.1f;
								typePrefixSO.healthMult += 0.12f;
								typePrefixSO.damageMult += 0.35f;
							}
						}

					}
				}

					SwingSpeed =	(int)(SwingSpeed * typePrefixSO.speedMult);
					Weight =		(int)(Weight * typePrefixSO.weightMult);
					WeaponHealth =	(int)(WeaponHealth * typePrefixSO.healthMult);

					DamageTypesAndValues[ElementsEnum.Physical] = new Vector2Int((int)(DamageTypesAndValues[ElementsEnum.Physical].x * typePrefixSO.damageMult),
																					(int)(DamageTypesAndValues[ElementsEnum.Physical].y * typePrefixSO.damageMult));
				
			}

			if (prefix != PrefixEnum.NULL)
			{
				Prefix prefixSO = AssetDatabase.LoadAssetAtPath<Prefix>("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Prefix/" + prefix.ToString() + ".asset");

				////public Weapon.PrefixEnum prefix;
				///
				if (prefixSO.addsNewDamage)
				{
					DamageTypesAndValues.Add(prefixSO.damageElement, new Vector2Int(prefixSO.DamageMin, prefixSO.DamageMax));
				}

				if (prefixSO.modifiesExistingDamage)
				{
					Vector2Int tempVec2Int;

					for (int i = 0; i < DamageTypesAndValues.Count; i++)
					{
						DamageTypesAndValues.TryGetValue((Item.ElementsEnum)i, out tempVec2Int);

						tempVec2Int = new Vector2Int((int)(tempVec2Int.x * prefixSO.damageModifier), (int)(tempVec2Int.y * prefixSO.damageModifier));

						DamageTypesAndValues[(Item.ElementsEnum)i] = tempVec2Int;
					}
				}
				if (prefixSO.isNegative && (int)rarity > 1) { rarity--; }

				if (prePrefix != PrePrefixEnum.NULL)
				{
					PrePrefix prePrefixSO = AssetDatabase.LoadAssetAtPath<PrePrefix>("Assets/ScriptableObjects/Weapons/WeaponPrefixes/PrePrefix/" + prePrefix.ToString() + ".asset");

					////public Weapon.PrePrefixEnum prePrefix;
					///

					Vector2Int modifiedValues = DamageTypesAndValues[prefixSO.damageElement];
					DamageTypesAndValues[prefixSO.damageElement] = new Vector2Int((int)(modifiedValues.x * prePrefixSO.effectMult), (int)(modifiedValues.y * prePrefixSO.effectMult));

					if (prefixSO.isNegative && (int)rarity > 1) { rarity--; }
				}
			}

			if (owner != OwnerEnum.NULL)
			{
				Owner ownerSO = AssetDatabase.LoadAssetAtPath<Owner>("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Owner/" + owner.ToString() + ".asset");

				if (ownerSO.isNegative && (int)rarity > 1) { rarity--; }
			}

			if (suffix != SuffixEnum.NULL)
			{
				Suffix suffixSO = AssetDatabase.LoadAssetAtPath<Suffix>("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Suffix/" + suffix.ToString() + ".asset");
				////public Weapon.SuffixEnum suffix;
				//// add in ability to cast special effects here
			}

			if (material != MaterialsEnum.NULL)
			{
				Materials materialSO = AssetDatabase.LoadAssetAtPath<Materials>("Assets/ScriptableObjects/Weapons/WeaponPrefixes/Materials/" + material.ToString() + ".asset");

				Weight = (int)(Weight * materialSO.WeightMult);
				SwingSpeed = (int)(SwingSpeed * materialSO.SpeedMult);
				WeaponHealth = (int)(WeaponHealth * materialSO.HealthMult);
				canRegen = (canRegen || materialSO.canRegen);

			}

			if (rarity != RarityLevelEnum.NULL)
			{
				RarityLevel raritySO = AssetDatabase.LoadAssetAtPath<RarityLevel>("Assets/ScriptableObjects/Weapons/WeaponPrefixes/RarityLevel/" + rarity.ToString() + ".asset");

				Vector2Int tempVec2Int;
				for (int i = 0; i < DamageTypesAndValues.Count; i++)
				{
					DamageTypesAndValues.TryGetValue((Item.ElementsEnum)i, out tempVec2Int);

					tempVec2Int = new Vector2Int((int)(tempVec2Int.x * raritySO.baseStatMultiplier), (int)(tempVec2Int.y * raritySO.baseStatMultiplier));

					DamageTypesAndValues[(Item.ElementsEnum)i] = tempVec2Int;
				}

				WeaponHealth = (int)(WeaponHealth * raritySO.baseStatMultiplier);
				//Weight =		(int)(Weight * raritySO.baseStatMultiplier);
				//SwingSpeed =	(int)(SwingSpeed * raritySO.baseStatMultiplier);
				bleedChance = (int)(bleedChance * raritySO.baseStatMultiplier * 0.15f);
			}
		}

		void BuildName()
		{
			name = "";
			shortName = "";

			if (owner != OwnerEnum.NULL)
			{ name += owner.ToString() + " "; }

			if (prePrefix != PrePrefixEnum.NULL)
			{ name += prePrefix.ToString() + " "; }

			if (prefix != PrefixEnum.NULL)
			{ name += prefix.ToString() + " "; }

			if (typePrefix == TypePrefixEnum.old || typePrefix == TypePrefixEnum.ancient || typePrefix == TypePrefixEnum.light
					|| typePrefix == TypePrefixEnum.heavy || typePrefix == TypePrefixEnum.curved || typePrefix == TypePrefixEnum.wide)
			{
				if (typePrefix != TypePrefixEnum.NULL)
				{
					name += typePrefix.ToString() + " ";
					shortName += typePrefix.ToString() + " ";
				}
				if (material != Item.MaterialsEnum.NULL)
				{ name += material.ToString() + " "; }
			}
			else
			{
				if (material != Item.MaterialsEnum.NULL)
				{ name += material.ToString() + " "; }
				if (typePrefix != TypePrefixEnum.NULL)
				{
					name += typePrefix.ToString() + " ";
					shortName += typePrefix.ToString() + " ";
				}
			}

			if (baseType != BaseTypeEnum.NULL)
			{
				name += baseType.ToString() + " ";
				shortName += baseType.ToString() + " ";
			}

			if (suffix != SuffixEnum.NULL)
			{ name += "of " + suffix.ToString() + " "; }

			name = ReplaceAll(name, '_', ' ');
			shortName = ReplaceAll(shortName, '_', ' ');
		}

		string ReplaceAll(string input, char target, char replaceWith)
		{
			StringBuilder sb = new StringBuilder(input.Length);

			for (int i = 0; i < input.Length; i++)
			{
				if (input[i] == target)
				{
					if (i > 0)
					{
						if (input[i - 1] != ' ')
						{
							sb.Append(replaceWith);
						}
					}
					else
					{ sb.Append(replaceWith); }
				}
				else
				{
					sb.Append(input[i]);
				}
			}

			return sb.ToString();
		}

		void ApplylevelMult()
		{
			for (int i = 0; i < DamageTypesAndValues.Count; i++)
			{
				if (DamageTypesAndValues[(Item.ElementsEnum)i].x > 0 || DamageTypesAndValues[(Item.ElementsEnum)i].y > 0)
				{

					DamageTypesAndValues[(Item.ElementsEnum)i] = new Vector2Int((int)(DamageTypesAndValues[(Item.ElementsEnum)i].x * SETTINGS.LevelMultiplier(itemLevel)),
																					(int)(DamageTypesAndValues[(Item.ElementsEnum)i].y * SETTINGS.LevelMultiplier(itemLevel)));
				}
			}
		}

		public enum BaseTypeEnum
		{
			NULL, sword, shield, bow, dagger, mace, hammer, log, cleaver, axe, hatchet, crossbow, knuckleduster, caestus, halberd, spear, pick, club
		}

		public enum TypePrefixEnum
		{
			NULL, great, old, battle, _long, _short, ancient, bastard, wide, curved, recurve, heavy, broad, light
		}

		public enum PrefixEnum
		{
			NULL, burning, haunted, blunt, broken, rusted, spiked, shocking, freezing, molten, mediocre, shonky, cheap, sprouting, charred,
					intimidating, thundering, zapping, cold, spooky, screaming, weakening, toxic, blessed
		}

		public enum PrePrefixEnum
		{
			NULL, awfully, terribly, extremely, grand, unbeleviably, uber, particularly, violently, definitely_not,
		}

		public enum OwnerEnum
		{
			NULL, Gorgons, Stevens, King_of_thieves, Poseidons, Devils_own, Beholders, Leftover
		}

		public enum SuffixEnum
		{
			NULL, Holy_Light, Fire_Burst, Acid_Splash
		}

		
	}

	[System.Serializable]
	public class DamageInfo
	{
		public Item.ElementsEnum element;
		public int minDamage;
		public int maxDamage;

		public DamageInfo(Item.ElementsEnum _element, int _minDamage, int _maxDamage)
		{
			element = _element;
			minDamage = _minDamage;
			maxDamage = _maxDamage;
		}
	}					  
}