﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IdleRPG
{
	public class Item
	{

		// would contain visual

		// name
		public string name;
		public string shortName;

		// item level?
		public int itemLevel;

		// elements (for damage, resistance, etc)
		public enum ElementsEnum
		{
			Physical, Psychic, Electric, Fire, Cold, Air, Spirit, Aural, Poison
		}

		//material types
		public enum MaterialsEnum
		{
			NULL, wood, bone, stone, darkstone, glass, obsidian, lead, copper, bronze, bismuth, iron, steel
		}

		// item rarity

		public enum RarityLevelEnum
		{
			NULL, common, uncommon, rare, veryRare, Legendary, Epic, Godly, Extraplanar //transcendant??
		}

		public enum RarityColoursEnum
		{
			white, green, blue, purple, orange, red, goldenGlowing, rainbowVoid
		}
	}
}