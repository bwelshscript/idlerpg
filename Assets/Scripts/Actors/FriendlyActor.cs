﻿using Homebrew;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IdleRPG
{
	public class FriendlyActor : Actor
	{

		[Foldout("UI Display", true)]
		public GameObject CharacterInfoPrefab;
		public CharacterInfo characterInfo;
		public Color HPEmptyCol, HPFullCol, MPEmptyCol, MPFullCol, SPDEmptyCol, SPDFullCol;


		public Text TurnTimerUI;


		public override void Init()
		{
			base.Init();
			characterInfo = Instantiate(CharacterInfoPrefab, BattleFieldController.Instance.CharacterInfoParent).GetComponent<CharacterInfo>();
			characterInfo.NameText.text = ActorName.Substring(0,8);
			HPText = characterInfo.HPText;
			MPText = characterInfo.MPText;
			TurnTimerUI = characterInfo.TurnText;

			characterInfo.CharacterPortrait.sprite = BattlefieldDisplay.sprite;
			characterInfo.CharacterPortrait.SetNativeSize();

			characterInfo.CharacterPortrait.rectTransform.sizeDelta *= 0.4f;
			characterInfo.CharacterPortrait.rectTransform.localPosition = characterInfo.CharacterPortrait.sprite.bounds.center;

			// TODO: TEMP TESTING OF ADDING UNITS TO BATTLEFIELD GRID
			int tryLimit = 15;
			bool success = false;
			do
			{
				BattlefieldTile bPos = new BattlefieldTile();
				bPos.row = (BattlefieldTileMaster.Row)Random.Range(0, 2);
				bPos.tile = (BattlefieldTileMaster.Tile)Random.Range(0, 2);

				BattlefieldTile[] battlefieldTiles = new BattlefieldTile[] { bPos };

				success = BattlefieldTileMaster.Instance.OccupyTiles(battlefieldTiles, this);
				tryLimit--;

				if (tryLimit <= 0) { success = true; }
			}
			while (!success);
			MoveCharacterDisplay();
		}


		public override void UIUpdate()
		{
			base.UIUpdate();

			// UPDATE TURN UI ELEMENTS ----------------------------------------

			string newSpdTxt = "";
			int bigPips = Mathf.FloorToInt(TURNTIMER / 16.6f);

			for (int i = 0; i < bigPips; i++)
			{
				newSpdTxt += "█";
			}
			for (int i = 0; i < 6 - bigPips; i++)
			{
				newSpdTxt += "- ";
			}

			if (bigPips >=6)
			{
				TurnTimerUI.color = SPDFullCol;
			}
			else
			{
				TurnTimerUI.color = SPDEmptyCol;
			}

			TurnTimerUI.text = newSpdTxt;

			// ----------------------------------------------------------------

		}

		public override void TurnUpdate()
		{
			base.TurnUpdate();


		}

		public override bool DoAction()
		{

			if (base.DoAction())
			{
				if (curTarget == null || curTarget.IsDead())
				{ curTarget = FindTarget(); }

				if (TryMove())
				{
					// keep running try move until it returns true, then allow continue
					if (curTarget == null) { return true; }
					curTarget.TakeDamage(GenerateDamageArray(), this);

					return true;
				}
				else
				{
					return false;
				}
			}

			return false;
		}

		public override Actor FindTarget()
		{
			 return	BattleFieldController.Instance.GetEnemyTargetRandom(BattleFieldController.TEAM.PARTY);
		}

		public override void UpdateHealthUI()
		{
			base.UpdateHealthUI();


			// UPDATE HEALTH UI ELEMENTS --------------------------------------
			// take health number, normalize to 0 -1 val
			HPText.text = _CurHEALTH.ToString();

			// take health number, normalize to 0 -1 val
			float healthPercent = (_CurHEALTH + 0.0f) / (HEALTH + 0.0f);

			HPText.color = Color.Lerp(HPEmptyCol, HPFullCol, healthPercent);
			// ----------------------------------------------------------------


		}

		public override void UpdateManaUI()
		{
			base.UpdateManaUI();

			MPText.text = _CurMANA.ToString();
			float manaPercent = (_CurMANA + 0.0f) / (MANA + 0.0f);

			MPText.color = Color.Lerp(MPEmptyCol, MPFullCol, manaPercent);
		}
	}
}