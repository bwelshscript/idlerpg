﻿using Homebrew;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IdleRPG
{

	public class EnemyActor : Actor
	{
		[Foldout("UI Display")] public int maxHealthPIPS;
		[Foldout("UI Display")] public RectTransform HPBGImage;
		[Foldout("UI Display")] public RectTransform HPAnchor;
		[Foldout("UI Display")] public Text nameDisplay;
		CanvasGroup HPCanvasGroup = new CanvasGroup();

		float HPFadeDelay = 0.5f;

		public bool displayBossHP = false;

		public override void Init()
		{
			base.Init();
			// SET HP BAR TYPE (NORMAL, BOSS) AND POSITION
			nameDisplay.text = ActorName;
			// TODO: DEFINE BOSS ENEMIES SO WE CAN SHOW NAME ONLY IF BOSS
			if (maxHealthPIPS < 50) { nameDisplay.gameObject.SetActive(false); }
			HPCanvasGroup = HPBGImage.parent.GetComponent<CanvasGroup>();

			// TODO: TEMP TESTING OF ADDING UNITS TO BATTLEFIELD GRID
			int tryLimit = 15;
			bool success = false;
			do
			{
				BattlefieldTile bPos = new BattlefieldTile();
				bPos.row = (BattlefieldTileMaster.Row)Random.Range(0, 2);
				bPos.tile = (BattlefieldTileMaster.Tile)Random.Range(7, 9);

				BattlefieldTile[] battlefieldTiles = new BattlefieldTile[] { bPos };

				success = BattlefieldTileMaster.Instance.OccupyTiles(battlefieldTiles, this);
				tryLimit--;

				if (tryLimit <= 0) { success = true; }
			}
			while (!success);
			MoveCharacterDisplay();
		}

		public override bool DoAction()
		{
			if (base.DoAction())
			{
				if (curTarget == null || curTarget.IsDead() )
				{ curTarget = FindTarget(); }

				if (TryMove())
				{
					// keep running try move until it returns true, then allow continue
					if (curTarget == null) { return true; }
					curTarget.TakeDamage(GenerateDamageArray(), this);

					return true;
				}
				else
				{
					return false;
				}
				
			}
			return false;
		}

		public override Actor FindTarget()
		{
			return BattleFieldController.Instance.GetEnemyTargetRandom(BattleFieldController.TEAM.NOTPARTY);
		}

		public override void UpdateHealthUI()
		{
			base.UpdateHealthUI();

			// UPDATE HEALTH UI ELEMENTS ----------------------------------------------------

			if (displayBossHP)
			{
				HPBGImage.transform.parent.position = BattleFieldController.Instance.BossHPAnchor.position;
				maxHealthPIPS = 55;
				nameDisplay.gameObject.SetActive(true);
			}
			else { HPBGImage.transform.parent.position = HPAnchor.position; }

			// make sure HP BG fits each pip, base size is 11 width + 5 per pip
			float HPBGSize = 11 + (5 * maxHealthPIPS);
			HPBGImage.sizeDelta = new Vector2(HPBGSize, 10);

			// take health number, normalize to 0 -1 val
			float healthPercent = (_CurHEALTH + 0.0f) / (HEALTH + 0.0f);

			// with percent, mult by max health pips
			// round down to get number of full pips
			int fullPips = Mathf.CeilToInt(maxHealthPIPS * healthPercent);

			// get float value by spllitting on '.' in a string

			string hp = "";

			for (int i = 0; i < fullPips; i++)
			{
				hp += "█  ";
			}

			for (int i = 0; i < (maxHealthPIPS) - fullPips; i++)
			{
				hp += "-    ";
			}

			HPText.text = hp;


			if (!displayBossHP && HPCanvasGroup.alpha > 0.25f)
			{
				if (HPFadeDelay >= 0) { HPFadeDelay -= Time.deltaTime; }
				else { HPCanvasGroup.alpha = Mathf.Lerp(HPCanvasGroup.alpha, 0, Time.deltaTime * 10); }
			}

			//----------------------------------------------------------------------------------

		}

		public override void TakeDamage(Damage[] damageArray, Actor source)
		{
			base.TakeDamage(damageArray, source);
			HPCanvasGroup.alpha = 1;
			HPFadeDelay = 0.5f;
		}
	}
}