﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterInfo : MonoBehaviour
{
	public Text HPText, MPText, TurnText, NameText;
	public Image CharacterPortrait;
}
