﻿using Homebrew;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace IdleRPG
{
	public class Actor : MonoBehaviour
	{
		// follow the same setup as the weapons
		// as we want to randomly generate these

		// needs stats

		[Foldout("Base Stats")] public int STR, DEX, INT, CONST, SPEED, ARMOUR, HEALTH,  MANA, MOVE;

		[Foldout("Current Stats")] public int curSTR, curDEX, curINT, curSPEED, curARMOUR, curMOVE;
		[SerializeField] private int curMANA;
		[SerializeField] private int curHEALTH;
		public int _CurHEALTH { get {  return curHEALTH; } set {  UpdateHealthUI(); curHEALTH = value; }  }
		public int _CurMANA { get { return curMANA; } set { UpdateManaUI(); curMANA = value; } }
		public int TURNTIMER = 0; // always max 100, increased by speed amt every turn (speed worked out by dex

		[Foldout("UI Display")] public string ActorName = "actor name";

		// contains the characters job/class, which is used by its AI to determine its turn actions


		[Foldout("UI Display")] public Text HPText,MPText;
		[Foldout("UI Display")] public Image BattlefieldDisplay, DamageOverlay;

		public Actor curTarget;

		[SerializeField, GetSet ("_resetCurStats")]
		public bool resetCurStats = false;
		public bool _resetCurStats
		{ get { return resetCurStats; } set
			{
				if (value)
				{
					ResetCurrentStats();
					_resetCurStats = false;
					resetCurStats = false;
				}
				resetCurStats = value;
			}
		}

		// display character name somewhere?

		// needs equipment slots

		// needs tile size, attacking tile and target tile, as well as a way to test if a tile we are moving to will keep the whole entity within the bounds of the battlefield

		// function to update text health bar any time the HP value changes


		int damageToReceive = 0;
		int TestingActionTimer = 20;
		bool canMove = true;
		public enum size { small, medium, large, giant }
		size mySize = size.small;

		List<Vector2Int> PathToTarget = new List<Vector2Int>();
		int movementAmt, currMovementAmount;

		Color currentColour =new Color(1,1,1,0); // for use with status effects, like poisoned

		// tile footprint is width then depth, from the main tile (front of actor). depth extends backwards from main tile
		Vector2Int tileFootprint = new Vector2Int(1, 1);

		// colour pulse values
		float convRange = 0, curPulseTime = 0, pulseTime = 0;

		private void Awake()
		{
			Init();
		}

		public virtual void Init()
		{
			BattleFieldController.Instance.ActorReport(this);
			TURNTIMER = curSPEED;
			DamageOverlay.sprite = BattlefieldDisplay.sprite;
			DamageOverlay.rectTransform.sizeDelta = BattlefieldDisplay.rectTransform.sizeDelta;
			DamageOverlay.color = currentColour;

			BattlefieldDisplay.material.enableInstancing = true;

			// TESTING RANDOM COLOURS
			BattlefieldDisplay.material.SetVector ("_HSVAAdjust", new Vector4(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f),0 ));
			BattlefieldDisplay.material.SetVector("_HSVAAdjust2", new Vector4(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0));
			BattlefieldDisplay.material.SetVector("_HSVAAdjust3", new Vector4(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0));
			BattlefieldDisplay.material.SetVector("_HSVAAdjust4", new Vector4(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0));

		}

		public virtual void Update()
		{
			if (curPulseTime > 0) { UpdatePulseActorColour(); }
		}

		public virtual void UIUpdate()
		{
			if (damageToReceive > 0)
			{
				// if the settings damage is greater than the amount of damage left to receive, just receive the left over amount of damage
				int dam = damageToReceive < SETTINGS.DAMAGE_VISUAL_AMT ? damageToReceive : SETTINGS.DAMAGE_VISUAL_AMT;

				damageToReceive -= dam;
				curHEALTH -= dam;
			}

			UpdateHealthUI();
			UpdateManaUI();

			

		}

		public virtual void TurnUpdate()
		{

			if (curHEALTH <= 0) { SetDead(); return; } // ACTOR IS DEAD, PLAY DEATH ANIM AND REMOVE FROM PLAY IF POSSIBLE

			// update turn timer
			TURNTIMER += curSPEED;

			if (TURNTIMER >= SETTINGS.TURNTIMER_MAX)
			{
				EnqueueAction();

			}
		}

		public virtual void TakeDamage(Damage[] damageArray, Actor source)
		{
			int damageAmt = 0;
			// add in
			for (int i = 0; i < damageArray.Length; i++)
			{
				damageAmt += (damageArray[i].DamageAmount - curARMOUR); // TODO: REPLACE THIS with fancy armour calculations, taking into account elemental resistances, etc.
			}

			// add in any other effects to received damage here, ie a buff/debuff that halves/doubles incoming damage

			damageToReceive += damageAmt;

			//Debug.Log(ActorName + " took " + damageAmt + " damage from " + source.ActorName);
			// damage to receive exists so we have a chance to display the damage taken over multiple frames.

			PulseActorColour(Color.red, 1.0f);

		}

		public virtual void EnqueueAction()
		{
			BattleFieldController.Instance.EnqueueAction(this);
			TestingActionTimer = 20; // TODO: replace the testing action timer with animating the character's movement and attacks, and then returning as true once they have finished.
		}

		public virtual bool DoAction()
		{
			
			// make sure nothing happens before this check, as the DoAction is run every frame of the action queue. this could be used to set up ripost and block attack interruptions.
			//if (TestingActionTimer > 0)
			//{
			//	TestingActionTimer--;
			//	return false;
			//}

			//Debug.Log(ActorName + " performed an action");
			// RUN BATTLE BEHAVIOUR, generally do movement, and attack a target within range, or block if possible, etc.
			TURNTIMER = 0;
			return true;
		}

		public virtual bool TryMove()
		{
			// test whether or not we /can/ move
			if (curMOVE <= 0) { return true; }

			if (PathToTarget.Count > 0) { return MoveAlongPath(); }
			// determine tile we would like to move to (and if this is already determined, would we like to change that tile)
			curTarget = FindTarget();

			Vector2Int tileInFrontOfTarget = GetTileInFrontOfTarget(BattlefieldTileMaster.Instance.GetTilesOwnedByActor(curTarget)[0]);

			// determine path to target tile
			PathToTarget = BattlefieldPathfinding.GetPath(new Vector2Int(3, 10),
				BattlefieldTileMaster.Instance.GetTilesOwnedByActor(this)[0],
				tileInFrontOfTarget,
				BattlefieldTileMaster.Instance.GetAllOccupiedTiles()).ToList<Vector2Int>();

			// try to move along that path, either curMove amount, or length of path
			movementAmt = PathToTarget.Count - 1 > curMOVE ? curMOVE : PathToTarget.Count - 1;

			// move path has been set up, return false so we start running movealongpath instead of setting the path
			return false;
		}

		private bool MoveAlongPath()
		{
			// move character;

			BattlefieldTile newPos = new BattlefieldTile();
			int index = 0;
			newPos.row = (BattlefieldTileMaster.Row)PathToTarget[index].x;
			newPos.tile = (BattlefieldTileMaster.Tile)PathToTarget[index].y;

			BattlefieldTile oldPos = BattlefieldTileMaster.Instance.GetTileRect( BattlefieldTileMaster.Instance.GetTilesOwnedByActor(this)[0]).GetComponent<TileInfo>().bTile; //jesus why did i do this

			// try to move to next tile
			if ( ! BattlefieldTileMaster.Instance.OccupyTiles(new BattlefieldTile[] { newPos }, this))
			{
				// cant move to next tile, stop moving here
				BattlefieldTileMaster.Instance.OccupyTiles(new BattlefieldTile[] { oldPos }, this);
				PathToTarget.Clear();
				return true;
			}

			PathToTarget.RemoveAt(index);

			MoveCharacterDisplay();
			movementAmt--;
			if (movementAmt < 0)
			{
				PathToTarget.Clear();
				return true;
			}

			
			return false;
		}

		Vector2Int GetTileInFrontOfTarget(Vector2Int targetTile)
		{
			Vector2Int myTile = BattlefieldTileMaster.Instance.GetTilesOwnedByActor(this)[0];
			int row = targetTile.x;
			int tile = targetTile.y > myTile.y ? targetTile.y - 1 : targetTile.y + 1;
			row = Mathf.Clamp(row, 0, 3);
			tile = Mathf.Clamp(tile, 0, 10);
			return new Vector2Int(row, tile);
		}

		public void MoveCharacterDisplay()
		{
			// move character display to be on top of a tile it occupies. TODO: update this to work for multiple tiles, and big charatcers
			Vector2Int moveTile = BattlefieldTileMaster.Instance.GetTilesOwnedByActor(this)[0];

			transform.position = BattlefieldTileMaster.Instance.GetTileRect(moveTile).position;
			// deal with sorting layer
			switch (moveTile.x)
			{
				case 1:
					transform.SetParent(BattleFieldController.Instance.BF_Rear_Layer);
					break;
				case 2:
					transform.SetParent(BattleFieldController.Instance.BF_Mid_Layer);
					break;
				case 3:
					transform.SetParent(BattleFieldController.Instance.BF_Front_Layer);
					break;
				default:
					break;
			}
		}

		public virtual void GetTileWithinRange( Actor target )
		{
			// oh dear, need to find a tile, closest to our current position, that would put our attack in range of our enemy target
		}

		public virtual Actor FindTarget()
		{
			// this should always be overridden. 
			// TODO : pass behaviour into this function that will then run specific target aquisition behaviour (like finding a friendly actor to protect or heal, instead of attack)
			return null;
		}

		public virtual void RestoreHeatlh()
		{
			PulseActorColour(Color.green, 1.15f);
		}

		public virtual void ReduceStat()
		{

		}

		public virtual void TakeMana()
		{

		}

		public virtual void RestoreMana()
		{

		}

		public bool IsDead()
		{
			if (curHEALTH > 0) { return false; }

			return true;
		}

		public virtual void SetDead()
		{
			currentColour = new Color(0.2f, 0.2f, 0.2f, 0.6f);
			PulseActorColour(Color.red, 1);
			BattleFieldController.Instance.ActorRemove(this);
			BattlefieldDisplay.color = new Color(0.2f, 0.2f, 0.2f, 0.2f);
		}

		public virtual void UpdateHealthUI()
		{
			curHEALTH = Mathf.Clamp(curHEALTH, 0, HEALTH);
		}

		public virtual void UpdateManaUI()
		{
			curMANA = Mathf.Clamp(curMANA, 0, MANA);
		}

		public virtual Damage[] GenerateDamageArray()
		{
			// base damage would be hitting, we would just use strength. this will be replaced with weapons or animal attacks like bite, etc.

			// PUNCH
			Damage punchDamage = new Damage();
			punchDamage.DamageAmount = curSTR;
			punchDamage.DamageElement = Item.ElementsEnum.Physical;
			return new Damage[] { punchDamage };
		}

		private void ResetCurrentStats()
		{
			curHEALTH = HEALTH;
			curMANA = MANA;
			curSPEED = SPEED;
			curARMOUR = ARMOUR;
			curSTR = STR;
			curDEX = DEX;
			curINT = INT;
		}

		public void PulseActorColour(Color pulseCol, float _pulseTime)
		{

			DamageOverlay.color = pulseCol;
			curPulseTime = _pulseTime;
			pulseTime = _pulseTime;
		}

		private void UpdatePulseActorColour()
		{
			// convert pulse time to a 0-1 range	

			curPulseTime -= Time.deltaTime;

			float OldRange = (pulseTime - 0f);
			float NewRange = (1f - 0f);
			float NewValue = (((curPulseTime - 0f) * NewRange) / OldRange) + 0f;

			DamageOverlay.color = Color.Lerp(DamageOverlay.color, currentColour, 1 - NewValue);
		}
	}

	public struct Damage
	{
		public Item.ElementsEnum DamageElement;
		public int DamageAmount;
	}

}
