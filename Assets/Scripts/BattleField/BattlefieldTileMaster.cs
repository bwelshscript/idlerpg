﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IdleRPG
{
	public class BattlefieldTileMaster : MonoBehaviour
	{
		private static BattlefieldTileMaster _instance;
		public static BattlefieldTileMaster Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<BattlefieldTileMaster>();
				}

				return _instance;
			}
		}

		public RectTransform BattlefieldRear, BattlefieldMid, BattlefieldFront;

		public RectTransform[] BattlefieldRearTiles, BattlefieldMidTiles, BattlefieldFrontTiles;

		public enum Tile { L1, L2, L3, L4, L5, R5, R4, R3, R2, R1 }
		public enum Row { Rear, Mid, Front }

		RectTransform[][] BattlefieldArray = new RectTransform[3][];
		Dictionary<Actor, TileInfo[]> currentlyOccupiedTiles = new Dictionary<Actor, TileInfo[]>();

		private void Init()
		{
			for (int i = 0; i < BattlefieldRearTiles.Length; i++)
			{
				BattlefieldRearTiles[i].gameObject.SetActive(false);
				BattlefieldMidTiles[i].gameObject.SetActive(false);
				BattlefieldFrontTiles[i].gameObject.SetActive(false);
			}

			// add the tile arrays to an array so we can convert a row & mid into an int,int index;

			BattlefieldArray[0] = BattlefieldRearTiles;
			BattlefieldArray[1] = BattlefieldMidTiles;
			BattlefieldArray[2] = BattlefieldFrontTiles;

			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 10; j++)
				{
					 BattlefieldArray[i][j].gameObject.AddComponent(typeof (TileInfo));
					TileInfo newTile = BattlefieldArray[i][j].GetComponent<TileInfo>();
					newTile.bTile.row = (Row)i;
					newTile.bTile.tile = (Tile)j;
					newTile.occupyingActor = null;
				}
			}
		}

		private RectTransform GetTile(Row row, Tile tile)
		{
			return BattlefieldArray[(int)row][(int)tile];
		}

		// create functions to check if tile is occupied
		 bool AreTilesClear(int row, int tile)
		{
			
			return IsTileClear(row, tile);
		}

		 bool AreTilesClear(Row row, Tile tile)
		{

			return IsTileClear((int)row, (int)tile);
		}

		 bool AreTilesClear(BattlefieldTile bTile)
		{

			return IsTileClear((int)bTile.row, (int)bTile.tile);
		}

		 bool AreTilesClear(BattlefieldTile[] bTiles)
		{

			foreach (BattlefieldTile bTile in bTiles)
			{
				if (!IsTileClear((int)bTile.row, (int)bTile.tile))
				{
					return false;
				}
			}

			return true;
		}

		// register tiles as occupied by actors
		public bool OccupyTiles(BattlefieldTile[] bTiles, Actor occupyingActor)
		{
			// first clear all tiles of actor we are trying to move. keep old position in case move fails
			TileInfo[] oldTiles = new TileInfo[0];
			List<BattlefieldTile> bTileList = new List<BattlefieldTile>();

			if (currentlyOccupiedTiles.ContainsKey(occupyingActor))
			{
				
				currentlyOccupiedTiles.TryGetValue(occupyingActor, out oldTiles);

				
				for (int i = 0; i < oldTiles.Length; i++)
				{
					bTileList.Add(oldTiles[i].bTile);
				}

				ClearTiles(bTileList.ToArray());
				currentlyOccupiedTiles.Remove(occupyingActor);
			}
			

			if (!AreTilesClear(bTiles))
			{
				// reset actors old occupied tiles
				currentlyOccupiedTiles.Add(occupyingActor, oldTiles);
				return false;
			}

			bTileList.Clear();
			List<TileInfo> newTiles = new List<TileInfo>();

			foreach (BattlefieldTile bTile in bTiles)
			{
				BattlefieldArray[(int)bTile.row][(int)bTile.tile].GetComponent<TileInfo>().occupyingActor = occupyingActor;
				newTiles.Add(BattlefieldArray[(int)bTile.row][(int)bTile.tile].GetComponent<TileInfo>());

				BattlefieldArray[(int)bTile.row][(int)bTile.tile].gameObject.SetActive(true);
			}

			// register new tiles to actor
			currentlyOccupiedTiles.Add(occupyingActor, newTiles.ToArray());
			return true;
		}

		public RectTransform GetTileRect(Vector2Int index)
		{
			return GetTile((Row)index.x, (Tile)index.y);
		}

		public Vector2Int[] GetTilesOwnedByActor(Actor actor)
		{
			TileInfo[] tileInfo = new TileInfo[0];
			currentlyOccupiedTiles.TryGetValue(actor, out tileInfo);
			List<Vector2Int> returnArr = new List<Vector2Int>();

			if (tileInfo == null)
			{
				 return null;
			}

			foreach (TileInfo tile in tileInfo)
			{
				returnArr.Add(new Vector2Int((int)tile.bTile.row, (int)tile.bTile.tile));
			}

			return returnArr.ToArray();
		}

		public Vector2Int[] GetAllOccupiedTiles()
		{
			TileInfo[] tileInfo = new TileInfo[0];
			List<Vector2Int> returnArr = new List<Vector2Int>();
			foreach (Actor key in currentlyOccupiedTiles.Keys)
			{
				currentlyOccupiedTiles.TryGetValue(key, out tileInfo);

				foreach (TileInfo tile in tileInfo)
				{
					returnArr.Add(new Vector2Int((int)tile.bTile.row, (int)tile.bTile.tile));
				}
			}
			return returnArr.ToArray();
		}

		// find way to fit actor sizes into tile grid

		// hook actor death in with clearing its occupied tiles
		public bool DeOccupyActorsTiles(Actor occupyingActor)
		{
			if (currentlyOccupiedTiles.ContainsKey(occupyingActor))
			{
				TileInfo[] oldTiles = new TileInfo[0];
				List<BattlefieldTile> bTileList = new List<BattlefieldTile>();
				currentlyOccupiedTiles.TryGetValue(occupyingActor, out oldTiles);

				for (int i = 0; i < oldTiles.Length; i++)
				{
					bTileList.Add(oldTiles[i].bTile);
				}

				ClearTiles(bTileList.ToArray());
				currentlyOccupiedTiles.Remove(occupyingActor);
			}

			return true;
		}

		bool IsTileClear( int row, int tile)
		{
			if (BattlefieldArray[row] == null) { Init(); }

			if (BattlefieldArray[row][tile].GetComponent<TileInfo>().occupyingActor == null) { return true; }
			return false;
		}

		void ClearTiles(BattlefieldTile[] bTiles)
		{
			foreach (BattlefieldTile bTile in bTiles)
			{
				BattlefieldArray[(int)bTile.row][(int)bTile.tile].GetComponent<TileInfo>().occupyingActor = null;
				BattlefieldArray[(int)bTile.row][(int)bTile.tile].gameObject.SetActive(false);
			}
		}
	}

	public class TileInfo : MonoBehaviour
	{
		public Actor occupyingActor;

		public BattlefieldTile bTile;
	}

	public struct BattlefieldTile
	{
		public BattlefieldTileMaster.Row row;
		public BattlefieldTileMaster.Tile tile;
	}
}