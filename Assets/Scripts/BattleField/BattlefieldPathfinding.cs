﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IdleRPG
{
	public static class BattlefieldPathfinding
	{

		static Vector2Int[][] GridParents;
		static int[][] GridGValues;
		static List<Vector2Int> openList ;
		static List<Vector2Int> closedList;

		static int InitVal = 99999999;
		static Vector2Int targetPos;

		// 0public function that takes a 2D grid size, a start point and end point, a list of obstacle points, and returns an array of points that describes a path from start to end
		// cannot take diagonal steps, cannot cross over itself, and cannot cross over obstacles.
		// if it cannot reach the end, then return a path that ends up closest to the target.

		public static Vector2Int[] GetPath(Vector2Int gridSize, Vector2Int StartPos, Vector2Int TargetPos, Vector2Int[] obstacleArr)
		{
			targetPos = TargetPos;
			//set up new grid to pathfind with
			GridParents = new Vector2Int[gridSize.x][];
			GridGValues = new int[gridSize.x][];

			for (int i = 0; i < gridSize.x; i++)
			{
				GridParents[i] = new Vector2Int[gridSize.y];
				GridGValues[i] = new int[gridSize.y];

				for (int j = 0; j < gridSize.y; j++)
				{
					GridParents[i][j] = new Vector2Int(-1, -1);
					GridGValues[i][j] = InitVal;
				}
			}



			openList = new List<Vector2Int>();
			closedList = new List<Vector2Int>();

			openList.Add(StartPos); // start by adding the original position to the open list

			// add obstacles to closed list
			foreach (Vector2Int pos in obstacleArr)
			{
				if (pos == TargetPos || pos == StartPos) { continue; } // dont accidentally add the start and end pos to closed
				closedList.Add(pos);
			}

			int maxIterations = 500;

			do {
				Vector2Int currentSquare = GetSquareWithLowestFScore(); // Get the square with the lowest F score

				closedList.Add(currentSquare); // add the current square to the closed list
				openList.Remove(currentSquare); // remove it to the open list

				if (closedList.Contains(targetPos) )
				{
					// PATH FOUND
					openList.Clear();
					break; // break the loop				
				}

				if ( maxIterations <= 0)
				{
					// PATH NOT FOUND
					Debug.LogWarning("could not complete path");
					openList.Clear();
					break; // break the loop				
				}

				CheckSurrounding(currentSquare);
				maxIterations--;

			} while (openList.Count > 0); // Continue until there is no more available square in the open list (which means there is no path)

			Vector2Int pathTile = targetPos;

			if (maxIterations <= 0)
			{
				// if we didnt get a path to the final pos, use closest tile as final pos
				pathTile = GetClosedSquareWithLowestFScore();
			}

			List<Vector2Int> returnArr = new List<Vector2Int>();
			// construct the return path

			if (pathTile != StartPos)
			{
				do
				{
					returnArr.Add(pathTile);

					if (pathTile.x < 0 || pathTile.x > 2 || pathTile.y < 0 || pathTile.y > 9)
					{
						Debug.LogWarning("outside grid bounds");
					}

					pathTile = GridParents[pathTile.x][pathTile.y]; // can add a bad path?
				}
				while (pathTile != StartPos);
			}
			else
			{
				returnArr.Add(pathTile);
			}

			returnArr.Reverse();

			
			return returnArr.ToArray(); // chage to returning path

		}

		static void CheckSurrounding(Vector2Int centerTile)
		{
			for (int i = -1; i <= 1; i++)
			{
				for (int j = -1; j <= 1; j++)
				{
					if (centerTile.x + i >= 0 && centerTile.x + i < GridParents.Length)
					{// can use i

						if (centerTile.y + j >= 0 && centerTile.y + j < GridParents[i +1].Length)
						{// can use j


							if (closedList.Contains(new Vector2Int(centerTile.x + i, centerTile.y + j)))
							{
								// ignore this tile
							}
							else
							{


								if (!openList.Contains(new Vector2Int(i, j)))
								{// not in open list, add to it and calc its value
									openList.Add(new Vector2Int(centerTile.x + i, centerTile.y + j));

									GridGValues[centerTile.x + i][centerTile.y + j] = GridGValues[centerTile.x][centerTile.y] + 1;

									GridParents[centerTile.x + i][centerTile.y + j] = new Vector2Int(centerTile.x, centerTile.y);

								}
								else
								{
									// check if the F score is lower than when we use the current path to get there, if its lower, update the score and its parent
								}

							}
						}
					}


				}
			}

		}

		static Vector2Int GetSquareWithLowestFScore()
		{
			int curScore = 999999999;
			Vector2Int returnVal = Vector2Int.zero;

			foreach (Vector2Int pos in openList)
			{
				int testedVal = CalculateScore(pos);
				if ( testedVal < curScore)
				{
					curScore = testedVal;
					returnVal = pos;
				}
			}

			return returnVal;
		}

		static Vector2Int GetClosedSquareWithLowestFScore()
		{
			int curScore = 999999999;
			Vector2Int returnVal = Vector2Int.zero;

			foreach (Vector2Int pos in closedList)
			{
				int testedVal = CalculateScore(pos);
				if (testedVal < curScore)
				{
					curScore = testedVal;
					returnVal = pos;
				}
			}

			return returnVal;
		}

		static int CalculateScore(Vector2Int startPos)
		{
			return Gscore(startPos) + Hscore(startPos);
		}

		static int Gscore(Vector2Int pos)
		{
			return GridGValues[pos.x][pos.y];
		}

		static int Hscore (Vector2Int startPos)
		{
			int minX = startPos.x < targetPos.x ? startPos.x : targetPos.x;
			int maxX = startPos.x > targetPos.x ? startPos.x : targetPos.x;
			int minZ = startPos.y < targetPos.y ? startPos.y : targetPos.y;
			int maxZ = startPos.y > targetPos.y ? startPos.y : targetPos.y;

			int distX = (maxX - minX);
			int distZ = (maxZ - minZ);

			int dist = Mathf.Abs(maxX - minX) + Mathf.Abs(maxZ - minZ);

			return dist;
		}
	}
}
		