﻿
using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnCOLOURSCHEME", order = 1)]
public class COLOURSCHEME : ScriptableObject
{
	public Color[] colours;
}
