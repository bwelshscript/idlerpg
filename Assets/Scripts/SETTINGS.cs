﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IdleRPG;

public static class SETTINGS
{
	public static int MAX_LEVEL = 100;
	public static int TURNTIMER_MAX = 110;
	public static int DAMAGE_VISUAL_AMT = 3;
	public static int TURNTIMER_UPDATE_SPEED = 15;
	public static int UI_UPDATE_TICKRATE = 10;

	public static Dictionary<Item.RarityLevelEnum, int> levels1to10 = new Dictionary<Item.RarityLevelEnum, int>()
	{	{ Item.RarityLevelEnum.common,			80},
		{ Item.RarityLevelEnum.uncommon,		15},
		{ Item.RarityLevelEnum.rare,			 5},
		{ Item.RarityLevelEnum.veryRare,		 0},
		{ Item.RarityLevelEnum.Legendary,		 0},
		{ Item.RarityLevelEnum.Epic,			 0},
		{ Item.RarityLevelEnum.Godly,			 0},
		{ Item.RarityLevelEnum.Extraplanar,		 0}
	};

	public static Dictionary<Item.RarityLevelEnum, int> levels11to31 = new Dictionary<Item.RarityLevelEnum, int>()
	{   { Item.RarityLevelEnum.common,          70},
		{ Item.RarityLevelEnum.uncommon,        15},
		{ Item.RarityLevelEnum.rare,            10},
		{ Item.RarityLevelEnum.veryRare,         4},
		{ Item.RarityLevelEnum.Legendary,        1},
		{ Item.RarityLevelEnum.Epic,             0},
		{ Item.RarityLevelEnum.Godly,            0},
		{ Item.RarityLevelEnum.Extraplanar,      0}
	};

	public static Dictionary<Item.RarityLevelEnum, int> levels31to51 = new Dictionary<Item.RarityLevelEnum, int>()
	{   { Item.RarityLevelEnum.common,          20},
		{ Item.RarityLevelEnum.uncommon,        45},
		{ Item.RarityLevelEnum.rare,            15},
		{ Item.RarityLevelEnum.veryRare,        10},
		{ Item.RarityLevelEnum.Legendary,        9},
		{ Item.RarityLevelEnum.Epic,             1},
		{ Item.RarityLevelEnum.Godly,            0},
		{ Item.RarityLevelEnum.Extraplanar,      0}
	};

	public static Dictionary<Item.RarityLevelEnum, int> levels51to71 = new Dictionary<Item.RarityLevelEnum, int>()
	{   { Item.RarityLevelEnum.common,           5},
		{ Item.RarityLevelEnum.uncommon,        10},
		{ Item.RarityLevelEnum.rare,            45},
		{ Item.RarityLevelEnum.veryRare,        25},
		{ Item.RarityLevelEnum.Legendary,        9},
		{ Item.RarityLevelEnum.Epic,             5},
		{ Item.RarityLevelEnum.Godly,            1},
		{ Item.RarityLevelEnum.Extraplanar,      0}
	};

	public static Dictionary<Item.RarityLevelEnum, int> levels71to101 = new Dictionary<Item.RarityLevelEnum, int>()
	{   { Item.RarityLevelEnum.common,           5},
		{ Item.RarityLevelEnum.uncommon,         5},
		{ Item.RarityLevelEnum.rare,            30},
		{ Item.RarityLevelEnum.veryRare,        35},
		{ Item.RarityLevelEnum.Legendary,       15},
		{ Item.RarityLevelEnum.Epic,             7},
		{ Item.RarityLevelEnum.Godly,            2},
		{ Item.RarityLevelEnum.Extraplanar,      1}
	};

	public static int LevelMultiplier (int level)
	{
		// divide by 10, round up and multiply by ten to get top end of each bracket (kept as 10's)
		level = Mathf.CeilToInt( level * 0.1f ) *10;
		if (level <= 0) { level = 1; }
		return LevelRangeMultiplier[level];
	}

	public static Dictionary<int, int> LevelRangeMultiplier = new Dictionary<int, int>()
	{
		{10,    1},
		{20,    2},
		{30,    4},
		{40,    5},
		{50,    6},
		{60,    8},
		{70,   10},
		{80,   13},
		{90,   15},
		{100,  25},
	};


	public enum ColourSchemes
	{
		Spring, Summer, Winter, Autum
	}


}
