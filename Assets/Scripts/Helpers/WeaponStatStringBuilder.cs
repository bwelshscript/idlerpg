﻿using UnityEngine;
using System.Reflection;
using System;
using IdleRPG;
using UnityEditor;

public static class WeaponStatStringBuilder
{
	public static string GetComponentString(Weapon myWeapon)
	{
		string retString = "";

		retString += "Level: \t\t\t\t\t\t" + myWeapon.itemLevel.ToString() + "\n";
		retString += "Rarity:\t\t\t\t\t\t" + myWeapon.rarity.ToString() + "\n";
		retString += "Health:\t\t\t\t\t\t" + myWeapon.WeaponHealth.ToString() + "\n";
		retString += "Weight:\t\t\t\t\t\t" + myWeapon.Weight.ToString() + "\n";
		retString += "Speed: \t\t\t\t\t\t" + myWeapon.SwingSpeed.ToString() + "\n";
		retString += "Reach: \t\t\t\t\t\t" + myWeapon.Reach.ToString() + "\n";
		retString += "AgeLvl:\t\t\t\t\t\t" + myWeapon.agedLevel.ToString() + "\n";
		retString += "bleed%:  \t\t\t\t\t" + myWeapon.bleedChance.ToString() + "\n";
		retString += "lvlMult:\t\t\t\t\t\t" + myWeapon.levelStatMultiplier.ToString() + "\n";
		retString += "2Hand: \t\t\t\t\t" + myWeapon.twoHanded.ToString() + "\n";
		retString += "canBleed:\t\t\t\t\t" + myWeapon.canBleed.ToString() + "\n";
		retString += "canRegen:\t\t\t\t" + myWeapon.canRegen.ToString() + "\n";
		retString += "mundane: \t\t\t\t" + myWeapon.mundane.ToString() + "\n";
		retString += "martial: \t\t\t\t\t" + myWeapon.martial.ToString() + "\n";
		retString += "_________________________________________________________" + "\n\n";
		// this is all damage types and values added together for easy display, not for working out damage
		retString += "Total Dmg: " + myWeapon.damageSummary.ToString() + "\n";
		retString += "_________________________________________________________" + "\n\n\n";
		foreach (DamageInfo item in myWeapon.damageInformation)
		{
			retString += "Dmg Element: \t\t " + item.element + "\n" +
				"\t\t\t\t-------------------------" +
				"\n\t\t\t\t Damage " + item.minDamage + "-" + item.maxDamage + "\n" +
				"\t\t\t\t-------------------------" + "\n\n";
		}
		return retString;
	}
}
