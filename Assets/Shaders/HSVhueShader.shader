﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/HSVRangeShader"
{
	Properties
	{
	   _MainTex("Sprite Texture", 2D) = "white" {}
	   _Color("Alpha Color Key", Color) = (0,0,0,1)
	   _HSVRangeMin("HSV Affect Range Min", Color) = (1,1,1,1)

	   _HSVRangeMax("HSV Affect Range Max",Color) = (1,1,1,1)
	   _HSVAAdjust("HSVA Adjust", Vector) = (0, 0, 0, 0)

	   _HSVRangeMin2("HSV Affect Range Min 2", Color) = (1,1,1,1)
	   _HSVRangeMax2("HSV Affect Range Max 2",Color) = (1,1,1,1)
	   _HSVAAdjust2("HSVA Adjust 2", Vector) = (0, 0, 0, 0)

	   _HSVRangeMin3("HSV Affect Range Min 3", Color) = (1,1,1,1)
	   _HSVRangeMax3("HSV Affect Range Max 3", Color) = (1,1,1,1)
	   _HSVAAdjust3("HSVA Adjust 3", Vector) = (0, 0, 0, 0)

	   _HSVRangeMin4("HSV Affect Range Min 4", Color) = (1,1,1,1)
	   _HSVRangeMax4("HSV Affect Range Max 4",Color) = (1,1,1,1)
	   _HSVAAdjust4("HSVA Adjust 4", Vector) = (0, 0, 0, 0)

	   _StencilComp("Stencil Comparison", Float) = 8
	   _Stencil("Stencil ID", Float) = 0
	   _StencilOp("Stencil Operation", Float) = 0
	   _StencilWriteMask("Stencil Write Mask", Float) = 255
	   _StencilReadMask("Stencil Read Mask", Float) = 255
	   _ColorMask("Color Mask", Float) = 15
	}
		SubShader
	   {
		   Tags
		   {
			   "RenderType" = "Transparent"
			   "Queue" = "Transparent"
		   }

		   Stencil
		   {
			   Ref[_Stencil]
			   Comp[_StencilComp]
			   Pass[_StencilOp]
			   ReadMask[_StencilReadMask]
			   WriteMask[_StencilWriteMask]
		   }
		   ColorMask[_ColorMask]

		   Pass
		   {
			   Cull Off
			   ZWrite Off
			   Blend SrcAlpha OneMinusSrcAlpha

			   CGPROGRAM
			   #pragma vertex vert
			   #pragma fragment frag
				#pragma multi_compile_instancing
				#include "UnityCG.cginc"

			   sampler2D _MainTex;
			  // float4 _Color;
			  // float4 _HSVRangeMin, _HSVRangeMin2,  _HSVRangeMin3,  _HSVRangeMin4;
			  // float4 _HSVRangeMax, _HSVRangeMax2,  _HSVRangeMax3,  _HSVRangeMax4;
			  // float4 _HSVAAdjust, _HSVAAdjust2,   _HSVAAdjust3,	_HSVAAdjust4;

			   struct Vertex
			   {
				   float4 vertex : POSITION;
				   float2 uv_MainTex : TEXCOORD0;
			   };

			   struct Fragment
			   {
				   float4 vertex : POSITION;
				   float2 uv_MainTex : TEXCOORD0;
			   };

			   UNITY_INSTANCING_BUFFER_START(Props)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVRangeMin)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVRangeMin2)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVRangeMin3)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVRangeMin4)
			   						 
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVRangeMax)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVRangeMax2)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVRangeMax3)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVRangeMax4)
			   						 
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVAAdjust)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVAAdjust2)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVAAdjust3)
			   UNITY_DEFINE_INSTANCED_PROP(float4, _HSVAAdjust4)
			   
			   UNITY_INSTANCING_BUFFER_END(Props)

			   Fragment vert(Vertex v)
			   {
				   Fragment o;

				   UNITY_SETUP_INSTANCE_ID(v);
				   UNITY_TRANSFER_INSTANCE_ID(v, o); // necessary only if you want to access instanced properties in the fragment Shader.

				   o.vertex = UnityObjectToClipPos(v.vertex);
				   o.uv_MainTex = v.uv_MainTex;

				   return o;
			   }

			   float3 rgb2hsv(float3 c) {
				 float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
				 float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
				 float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

				 float d = q.x - min(q.w, q.y);
				 float e = 1.0e-10;
				 return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
			   }

			   float3 hsv2rgb(float3 c) {
				 c = float3(c.x, clamp(c.yz, 0.0, 1.0));
				 float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
				 float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
				 return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
			   }

			   float4 frag(Fragment IN) : COLOR
			   {
						UNITY_SETUP_INSTANCE_ID(i); // necessary only if any instanced properties are going to be accessed in the fragment Shader.

				   float4 o = float4(1, 0, 0, 0.2);



				   float4 color = tex2D(_MainTex, IN.uv_MainTex);
				   float3 hsv = rgb2hsv(color.rgb);
				   float affectMult  = step(rgb2hsv(UNITY_ACCESS_INSTANCED_PROP(Props, _HSVRangeMin )).x,  hsv.r) * step(hsv.r, rgb2hsv(UNITY_ACCESS_INSTANCED_PROP(Props,_HSVRangeMax )).x);
				   float affectMult2 = step(rgb2hsv(UNITY_ACCESS_INSTANCED_PROP(Props, _HSVRangeMin2)).x, hsv.r) * step(hsv.r,  rgb2hsv(UNITY_ACCESS_INSTANCED_PROP(Props,_HSVRangeMax2)).x);
				   float affectMult3 = step(rgb2hsv(UNITY_ACCESS_INSTANCED_PROP(Props, _HSVRangeMin3)).x, hsv.r) * step(hsv.r,  rgb2hsv(UNITY_ACCESS_INSTANCED_PROP(Props,_HSVRangeMax3)).x);
				   float affectMult4 = step(rgb2hsv(UNITY_ACCESS_INSTANCED_PROP(Props, _HSVRangeMin4)).x, hsv.r) * step(hsv.r,  rgb2hsv(UNITY_ACCESS_INSTANCED_PROP(Props,_HSVRangeMax4)).x);
				   float3 rgb =( hsv2rgb(hsv + UNITY_ACCESS_INSTANCED_PROP(Props, _HSVAAdjust.xyz ) * affectMult)
							   + hsv2rgb(hsv + UNITY_ACCESS_INSTANCED_PROP(Props, _HSVAAdjust2.xyz) * affectMult2)
							   + hsv2rgb(hsv + UNITY_ACCESS_INSTANCED_PROP(Props, _HSVAAdjust3.xyz) * affectMult3)
							   + hsv2rgb(hsv + UNITY_ACCESS_INSTANCED_PROP(Props, _HSVAAdjust4.xyz) * affectMult4)) * 0.25;



				   return float4(rgb, color.a + _HSVAAdjust.a);
			   }

			   ENDCG
		   }
	   }
}