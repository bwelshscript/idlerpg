﻿Shader "Sprites/Masked"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}

		_ColorPrimFROM("Primary Color (R)", Color) = (1,1,1,1)
		_ColorSecFROM("Secondary Color (G)", Color) = (1,1,1,1)
		_ColorTertFROM("Tertiary Color (B)", Color) = (1,1,1,1)
		_ColorPrimTO("Primary Color (R)", Color) = (1,1,1,1)
		_ColorSecTO("Secondary Color (G)", Color) = (1,1,1,1)
		_ColorTertTO("Tertiary Color (B)", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		[MaterialToggle] KEEPBLACK("Keep Black Pixels", Float) = 0

			// stencil for UI integration
			[HideInInspector] _StencilComp("Stencil Comparison", Float) = 8
			[HideInInspector] _Stencil("Stencil ID", Float) = 0
			[HideInInspector] _StencilOp("Stencil Operation", Float) = 0
			[HideInInspector] _StencilWriteMask("Stencil Write Mask", Float) = 255
			[HideInInspector] _StencilReadMask("Stencil Read Mask", Float) = 255

			[HideInInspector] _ColorMask("Color Mask", Float) = 15
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			Blend One OneMinusSrcAlpha

			Pass
			{
					 Stencil
					{
						Ref[_Stencil]
						Comp[_StencilComp]
						Pass[_StencilOp]
						ReadMask[_StencilReadMask]
						WriteMask[_StencilWriteMask]
					}
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile _ PIXELSNAP_ON
				#pragma multi_compile _ KEEPBLACK_ON
				#include "UnityCG.cginc"

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					fixed4 color : COLOR;
					float2 texcoord  : TEXCOORD0;
				};

				fixed4 _Color;

				v2f vert(appdata_t IN)
				{
					v2f OUT;
					OUT.vertex = UnityObjectToClipPos(IN.vertex);
					OUT.texcoord = IN.texcoord;
					OUT.color = IN.color * _Color;
					#ifdef PIXELSNAP_ON
					OUT.vertex = UnityPixelSnap(OUT.vertex);
					#endif

					return OUT;
				}

				sampler2D _MainTex;
				sampler2D _AlphaTex;
				float _AlphaSplitEnabled;
				float4 _ColorPrimFROM, _ColorSecFROM, _ColorTertFROM, _ColorPrimTO, _ColorSecTO, _ColorTertTO;

				fixed4 SampleSpriteTexture(float2 uv)
				{
					fixed4 color = tex2D(_MainTex, uv);


		#if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
						if (_AlphaSplitEnabled)
							color.a = tex2D(_AlphaTex, uv).r;
		#endif //UNITY_TEXTURE_ALPHASPLIT_ALLOWED

						return color;
					}

					fixed4 frag(v2f IN) : SV_Target
					{
						// sprite texture
						fixed4 c = SampleSpriteTexture(IN.texcoord);

					// all color channels multiplied with a new color
						float4 colorPrimFROM =  (c.r * _ColorPrimFROM);
						float4 colorSecFROM = (c.g * _ColorSecFROM);
						float4 colorTertFROM = (c.b * _ColorTertFROM);
						// added together
						float4 colorResult = colorPrimFROM + colorSecFROM + colorTertFROM;

						colorResult.a = 1;
						// keep black color for outlines
		#ifdef KEEPBLACK_ON
						colorResult += c * (1 - (c.r + c.g + c.b));
		#endif
						// multiply with alpha
						colorResult *= c.a;
						return colorResult;
					}
				ENDCG
				}
		}
}